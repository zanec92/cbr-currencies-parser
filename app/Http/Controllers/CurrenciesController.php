<?php

namespace App\Http\Controllers;

use App\Currency;

class CurrenciesController extends Controller
{
    public function all() {
        $currencies = Currency::all();
        return view('currencies.list', compact('currencies'));
    }

    public function getById($id) {
        $currency = Currency::whereCurrencyId($id)->first();
        return view('currencies.info', compact('currency'));
    }
}
