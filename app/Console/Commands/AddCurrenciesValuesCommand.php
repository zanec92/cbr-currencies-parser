<?php

namespace App\Console\Commands;

use App\Jobs\AddCurrenciesValuesJob;
use Illuminate\Console\Command;

class AddCurrenciesValuesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Получить значения валют с cbr.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AddCurrenciesValuesJob::dispatch();
    }
}
