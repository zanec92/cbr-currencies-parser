<?php

namespace App\Console\Commands;

use App\Jobs\UpdateCurrenciesValuesJob;
use Illuminate\Console\Command;

class UpdateCurrenciesValuesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить значения валют с cbr.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        UpdateCurrenciesValuesJob::dispatch();
    }
}
