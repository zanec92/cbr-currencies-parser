<?php

namespace App\Jobs;

use App\Currency;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddCurrenciesValuesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $client;

    protected $dailyCurrencies;

    protected $dailyCurrenciesEng;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->dailyCurrencies = 'http://www.cbr.ru/scripts/XML_daily.asp';
        $this->dailyCurrenciesEng = 'http://www.cbr.ru/scripts/XML_daily_eng.asp';
    }

    public function getCurrencies($url) {
        $client = new Client;
        $response = $client->get($url);
        $response = $response->getBody()->getContents();

        return simplexml_load_string($response);
    }

    /**
     * @param Client $client
     */
    public function handle()
    {
        $currencies = $this->getCurrencies($this->dailyCurrencies);

        foreach ($currencies->Valute as $currency) {
            Currency::create([
                'currency_id' => $currency->attributes()->ID,
                'name' => $currency->Name,
                'alphabetic_code' => $currency->CharCode,
                'digit_code' => $currency->NumCode,
                'rate' => floatval(str_replace(',', '.', $currency->Value))
            ]);
        }

        $currenciesEng = $this->getCurrencies($this->dailyCurrenciesEng);

        foreach ($currenciesEng->Valute as $currency) {
            Currency::whereCurrencyId($currency->attributes()->ID)
                ->update(['english_name' => $currency->Name]);
        }

    }

}
