<?php

namespace App\Jobs;

use App\Currency;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCurrenciesValuesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $client;

    protected $dailyCurrencies;

    protected $dailyCurrenciesEng;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->dailyCurrencies = 'http://www.cbr.ru/scripts/XML_daily.asp';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client;
        $response = $client->get($this->dailyCurrencies);
        $response = $response->getBody()->getContents();

        $currencies = simplexml_load_string($response);

        foreach ($currencies->Valute as $currency) {
            Currency::whereCurrencyId($currency->attributes()->ID)
                ->update(['rate' => floatval(str_replace(',', '.', $currency->Value))]);

        }
    }
}
