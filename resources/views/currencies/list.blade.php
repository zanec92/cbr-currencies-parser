@extends('currencies.template')

@section('content')

    <ol>

        @foreach($currencies as $currency)

                <li><a href="{{ route('currency.info', ['id' => $currency->currency_id]) }}">{{ $currency->name }} ({{ $currency->english_name }})</a></li>

        @endforeach

    </ol>

@endsection